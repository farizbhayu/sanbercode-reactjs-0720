// Soal 1. 

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var kata = kataPertama.concat(" ", kataKedua.charAt(0).toUpperCase() + kataKedua.slice(1), " ", kataKetiga, " ",  kataKeempat.toUpperCase());
console.log(kata);

// Soal 2. 

var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var total = parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga) + parseInt(kataKeempat);
console.log(total);

// Soal 3.

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substr(4, 10); 
var kataKetiga = kalimat.substr(15, 3); 
var kataKeempat = kalimat.substr(19, 5); 
var kataKelima = kalimat.substr(25, 6);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// Soal 4. 

var nilai = 90;
var kata = " indeksnya";

if (nilai >= 0 && nilai <= 100) {
  if (nilai >= 80) {
    console.log(nilai + kata + " A");
  } else if (nilai >=70 && nilai < 80) {
    console.log(nilai + kata + " B");
  } else if (nilai >= 60 && nilai < 70){
    console.log(nilai + kata + " C");
  } else if (nilai >= 50 && nilai < 60) {
    console.log(nilai + kata + " D");
  } else {
    console.log(nilai + kata + " E");
  }
} else {
  console.log("Nilai Salah");
}

// Soal 5. 

var tanggal = 12;
var bulan = 4;
var tahun = 1996;

switch(bulan) {
  case 1: 
  {
    bulan = "Januari";
    break;
  }
  case 2: 
  {
    bulan = "Februari";
    break;
  }
  case 3: 
  {
    bulan = "Maret";
    break;
  }
  case 4: 
  {
    bulan = "April";
    break;
  }
  case 5: 
  {
    bulan = "Mei";
    break;
  }
  case 6: 
  {
    bulan = "Juni";
    break;
  }
  case 7: 
  {
    bulan = "Juli";
    break;
  }
  case 8: 
  {
    bulan = "Agustus";
    break;
  }
  case 9: 
  {
    bulan = "September";
    break;
  }
  case 10: 
  {
    bulan = "Oktober";
    break;
  }
  case 11: 
  {
    bulan = "November";
    break;
  }
  case 12: 
  {
    bulan = "Desember";
    break;
  }
  default: 
  {
    bulan = "Invalid";
  }
}

console.log(tanggal + " " + bulan + " " + tahun);
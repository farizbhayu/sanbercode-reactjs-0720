// Soal 1.
console.log("LOOPING PERTAMA");
var counter = 1;

while(counter <= 20){
  if(counter%2==0){
    console.log(counter + " - I Love Coding");
  }
  counter++;
}

console.log("LOOPING KEDUA");
var counter = 20;

while(counter >= 1){
  if(counter%2==0){
    console.log(counter + " - I will become a front end developer");
  }
  counter--;
}

// Soal 2.
for(angka = 1; angka <=20; angka++){
    if(angka%3==0 && angka%2==1){
        console.log(angka + " - I Love Coding");
    } else if(angka%2==0) {
        console.log(angka + " - Berkualitas");
    } else if(angka%2==1){
        console.log(angka + " - Santai");
    } else {
        console.log("nothing");
    }
}

// Soal 3.
for(tagar = "#"; tagar.length < 8; tagar += "#"){
    console.log(tagar);
}

// Soal 4.
var kalimat="saya sangat senang belajar javascript"
var arrKalimat = kalimat.split(" ");
console.log(arrKalimat);

// Soal 5.
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();
for(i = 0; i <= daftarBuah.length-1; i++){
  console.log(daftarBuah[i]);
}
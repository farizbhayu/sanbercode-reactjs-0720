// Soal 1.
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992];

var objDaftarPeserta = 
{
  nama: "Asep",
  "jenis kelamin": "Laki-laki",
  hobi: "baca buku",
  "tahun lahir": 1992
}
console.log(objDaftarPeserta); 

// Soal 2.
var buah = 
[
  {
    nama: "strawberry",
    warna: "merah",
    "ada bijinya": "tidak",
    harga: 9000
  },
  {
    nama: "jeruk",
    warna: "oranye",
    "ada bijinya": "ada",
    harga: 8000
  },
  {
    nama: "Semangka",
    warna: "Hijau & merah",
    "ada bijinya": "ada",
    harga: 10000
  },
  {
    nama: "Pisang",
    warna: "Kuning",
    "ada bijinya": "tidak",
    harga: 5000
  }
]
console.log(buah[0]); 

// Soal 3.
var dataFilm = [];

function tambahData(nama, durasi, genre, tahun){
  dataFilm.push({
      nama: nama,
      durasi: durasi,
      genre: genre,
      tahun: tahun
  });
}
tambahData("Nightcrawler", "01:57:53", "Crime", 2014);
console.log(dataFilm);

// Soal 4.
// Realease 0
class Animal {
  constructor(name){
    this.name = name;
    this.legs = 4;
    this.cold_bloodded = false;
  }
  }
  var sheep = new Animal("shaun");
  
  console.log(sheep.name) // "shaun"
  console.log(sheep.legs) // 4
  console.log(sheep.cold_bloodded) // false
  
// Realease 1
class Ape extends Animal {
  constructor(name){
      super(name)
      this.legs = 2;
  }

  yell(){
      return "Auooo \n"+this.name+" \n"+this.legs+" \n"+this.cold_bloodded;
  }
}

class Frog extends Animal {
  constructor(name){
      super(name)
  }
  jump(){
      return "hop hop \n"+this.name+" \n"+this.legs+" \n" +this.cold_bloodded;
  }
}

var sungokong = new Ape("kera sakti");
console.log(sungokong.yell()); // "Auooo"

var kodok = new Frog("buduk");
console.log(kodok.jump()); // "hop hop" 

// Soal 5.
class Clock {
  constructor({ template }){
    var timer;

    function render() {
      var date = new Date();

      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;

      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;

      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;

      var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

      console.log(output);
    }
    this.stop = function (){
        clearInterval(timer);
    };
    this.start = function() {
        render();
        timer = setInterval(render, 1000);
    };
  }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  